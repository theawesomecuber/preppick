import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Think from '../views/Game/Think.vue'
import Vote from '../views/Game/Vote.vue'
import Results from '../views/Results.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/game/think',
    name: 'think',
    component: Think,
    props: true
  },
  {
    path: '/game/vote',
    name: 'vote',
    component: Vote
  },
  {
    path: '/results',
    name: '',
    component: Results
  },
]

const router = new VueRouter({
  routes
})

export default router
